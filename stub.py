# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import calendar
import time
import random
import string
from datetime import datetime

from operator import methodcaller

from alphalogic_api import options
from alphalogic_api.objects import Root, Object
from alphalogic_api.attributes import Visible, Access
from alphalogic_api.objects import ParameterBool, ParameterLong, ParameterDouble, ParameterDatetime, ParameterString
from alphalogic_api.decorators import command, run
from alphalogic_api.logger import log
from alphalogic_api import init


class RandomAdapter(Root):
    def handle_get_available_children(self):
        return [
            (RandomImage, 'RandomImage'),
            (RandomString, 'RandomString'),
            (RandomDateTime, 'RandomDateTime'),
        ]


#  Random Image node
def random_image(rnd_image):
    base_url = 'https://picsum.photos'

    args = []
    if rnd_image.ImageRandom.val:
        args.append('random')
    if rnd_image.ImageSpecific.val:
        args.append('image={}'.format(rnd_image.ImageSpecificId.val))
    if rnd_image.ImageBlur.val:
        args.append('blur')
    if rnd_image.ImageGravity.val:
        args.append('gravity={}'.format(rnd_image.IMAGE_GRAVITY[rnd_image.ImageGravityValue.val][1]))
    if rnd_image.ImageHash.val:
        args.append('hash={}{}'.format(calendar.timegm(time.gmtime()), datetime.now().microsecond))
    if rnd_image.ImageGrayscale.val:
        base_url += '/g'
    arguments = '&'.join(args)

    url = '{}/{}/{}/?{}'.format(base_url, rnd_image.ImageWidth.val, rnd_image.ImageHeight.val, arguments)

    return url


class RandomImage(Object):
    IMAGE_GRAVITY = (
        (0, 'center'),
        (1, 'east'),
        (2, 'south'),
        (3, 'west'),
        (4, 'north'),
    )

    ImageValue = ParameterString(visible=Visible.runtime, default='')
    ImageWidth = ParameterLong(visible=Visible.runtime, default=400)
    ImageHeight = ParameterLong(visible=Visible.runtime, default=400)
    ImageRandom = ParameterBool(visible=Visible.runtime, default=True)
    ImageGrayscale = ParameterBool(visible=Visible.runtime, default=False)
    ImageSpecific = ParameterBool(visible=Visible.runtime, default=False)
    ImageSpecificId = ParameterLong(visible=Visible.runtime, default=1)
    ImageBlur = ParameterBool(visible=Visible.runtime, default=False)
    ImageGravity = ParameterBool(visible=Visible.runtime, default=False)
    ImageGravityValue = ParameterLong(visible=Visible.runtime, choices=IMAGE_GRAVITY, default=0)
    ImageHash = ParameterBool(visible=Visible.runtime, default=True)

    @run(period_update=1)
    def run_update(self):
        self.ImageValue.val = random_image(self)

    @command(result_type=unicode)
    def RandomImage(self):
        return random_image(self)


#  Random String node
def random_string(rnd_string):
    if not any((rnd_string.StringLetters.val, rnd_string.StringDigits.val, rnd_string.StringWhitespace.val)):
        rnd_string.error.val = True
        rnd_string.status.val = 'No values for choice!'
        return ""
    else:
        if rnd_string.error.val:
            rnd_string.error.val = False
        if rnd_string.status.val:
            rnd_string.status.val = ''

    settings = ([])
    if rnd_string.StringLetters.val:
        settings.append(string.letters)
    if rnd_string.StringDigits.val:
        settings.append(string.digits)
    if rnd_string.StringWhitespace.val:
        settings.append(string.whitespace)
    if not len(settings):
        return ''

    value = ''.join(item for item in settings)
    string_value = ''.join(random.choice(value) for i in range(rnd_string.StringLength.val))

    if rnd_string.StringTransform.val == 1:
        string_value = string_value.capitalize()
    elif rnd_string.StringTransform.val == 2:
        string_value = string_value.upper()
    elif rnd_string.StringTransform.val == 3:
        string_value = string_value.lower()

    return string_value


class RandomString(Object):
    STRING_TRANSFORM = (
        (0, 'Random'),
        (1, 'Capitalize'),
        (2, 'Uppercase'),
        (3, 'Lowercase'),
    )

    StringValue = ParameterString(visible=Visible.runtime, default='')
    StringLength = ParameterLong(visible=Visible.runtime, default=10)
    StringLetters = ParameterBool(visible=Visible.runtime, default=True)
    StringDigits = ParameterBool(visible=Visible.runtime, default=True)
    StringWhitespace = ParameterBool(visible=Visible.runtime, default=False)
    StringTransform = ParameterLong(visible=Visible.runtime, choices=STRING_TRANSFORM, default=0)

    @run(period_update=1)
    def run_update(self):
        self.StringValue.val = random_string(self)

    @command(result_type=unicode)
    def RandomString(self):
        return random_string(self)


#  Random DateTime node
def random_datetime():
    return random.randint(0, 32536846799)


def current_datetime(rnd_datetime):
    return int(time.mktime(time.strptime(time.strftime(rnd_datetime.DateTimeFormat.default.format()), rnd_datetime.DateTimeFormat.default)))


class RandomDateTime(Object):
    DateTimeCurrent = ParameterBool(visible=Visible.runtime, default=False)
    DateTimeValue = ParameterLong(visible=Visible.runtime, default=0)
    DateTimeString = ParameterString(visible=Visible.runtime, default='')
    DateTimeFormat = ParameterString(visible=Visible.runtime, default="%Y-%m-%d %H:%M:%S")
    DateTimeTimestamp = ParameterString(visible=Visible.runtime, default='')

    @run(datetime_update=1)
    def run_datetime_update(self):
        if self.DateTimeCurrent.val:
            dt_str = time.strftime(self.DateTimeFormat.val.format())
            dt_int = current_datetime(self)
        else:
            dt_int = random_datetime()
            dt_str = time.strftime(str(self.DateTimeFormat.val), time.gmtime(dt_int))

        self.DateTimeString.val = dt_str
        self.DateTimeValue.val = dt_int

    @run(timestamp_update=0.5)
    def run_timestamp_update(self):
        self.DateTimeTimestamp.val = ('{}{}'.format(calendar.timegm(time.gmtime()), datetime.now().microsecond))

    @command(result_type=int)
    def RandomDateTime(self):
        return random_datetime()

    @command(result_type=int)
    def CurrentDateTime(self):
        return current_datetime(self)


if __name__ == '__main__':
    root = RandomAdapter()
    root.join()